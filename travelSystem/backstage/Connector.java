package travelSystem.backstage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// 這是個專門用來做連接Database的Class
public class Connector {
	public Connection con;

	public Connector() throws ClassNotFoundException, SQLException {
		// 連結 database "trip" in user Ben
		Class.forName("com.mysql.cj.jdbc.Driver"); // 先加載驅動器
		String url = "jdbc:mysql://192.168.43.18:8888/trip?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true"; // 數據庫名稱
		String username = "Ben"; // 使用者名稱
		String password = "Zz7181212"; // 密碼
		this.con = DriverManager.getConnection(url, username,password); // 使用這個接口連接數據庫。
		//System.out.println("connection trip success");
	}

	public Connector(String database) throws ClassNotFoundException, SQLException {
		//  連結 database "database" in user Ben
		Class.forName("com.mysql.cj.jdbc.Driver"); // 先加載驅動器
		String url = "jdbc:mysql://192.168.43.18:8888/" + database + "?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true"; // 數據庫名稱
		String username = "Ben"; // 使用者名稱
		String password = "Zz7181212"; // 密碼
		this.con = DriverManager.getConnection(url, username,password); // 使用這個接口連接數據庫。
		//System.out.println("connection " + database +  " success");
	}


}
