package travelSystem.backstage;

public class Data {
	public String title;
	public String travelcode;
	public String travelkey;
	public int price;
	public String startday;
	public String endday;
	public int lowerbound;
	public int upperbound;
	public int booking_id;

	public Data(String s1, String s2, String s3, int i1, String s4, String s5, int i2, int i3) {
		this.title = s1;
		this.travelcode = s2;
		this.travelkey = s3;
		this.price = i1;
		this.startday = s4;
		this.endday = s5;
		this.lowerbound = i2;
		this.upperbound = i3;
		this.booking_id = 0;
	}

	public Data(Data d) {
		this.title = d.title;
		this.travelcode = d.travelcode;
		this.travelkey = d.travelkey;
		this.price = d.price;
		this.startday = d.startday;
		this.endday = d.endday;
		this.lowerbound = d.lowerbound;
		this.upperbound = d.upperbound;
		this.booking_id = d.booking_id;
	}

	public Data() {

	}
}
