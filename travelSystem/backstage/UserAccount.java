package travelSystem.backstage;

import travelSystem.execution.*;
import travelSystem.execution.exception.*;

import java.io.*;
import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserAccount {

	private String email_id;
	public boolean Check;
	private String email;
	private String user_name;

	public String Get_user_name() {
		return user_name;
	}

	public String Get_user_email() {
		return this.email;
	}

	public UserAccount() throws ClassNotFoundException, SQLException, IOException {
		// 這是給沒有帳戶的人設立帳戶使用的

		// connect database "user_account" in user "Ben"
		Connector Con = new Connector("user_account");
		Create_Account(Con);
	}

	public UserAccount(String email, String password) throws ClassNotFoundException, SQLException, IOException {
		// connect database "user_account" in user "Ben"
		Connector Con = new Connector("user_account");
		int j = CheckAccount(email, password, Con);
		if (this.Check == true) {
		}
		else {
			InputStreamReader ir = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(ir);

			if (j == 1) {
				System.out.println("Do you want to create a new account?(y/n)");
				String str = in.readLine();
				if (str.equals("y")) {
					Create_Account(Con);
				}
				else {
					System.out.println("See you next time!");
					System.exit(0);
				}
			}

			else {
				int i = 1;
				while (i != 0) { //只要不對就繼續輸入
					System.out.println("Please enter your password again! (To leave, enter 'exit'.)");
					String ss = in.readLine();
					if (ss.equals("exit")) {
						System.out.println("See you next time!");
						System.exit(0);
					}

					else {
						password = ss;
						i = CheckAccount(email, password, Con);
					}

				}
			}
		}
	}

	public int CheckAccount(String email, String password, Connector Con) throws SQLException {
		String email_id = email.split("@")[0];

		ResultSet rs  = Con.con.getMetaData().getTables(null, null, email_id, null );// 找出這個email_id是否被使用過
		if (rs.next() == false) {
			System.out.println("The account does not exist!");
			this.Check = false;
			return 1;
		}
		else {
			String sqlser = "select * from " + email_id + " where password is not null;";
			PreparedStatement sta1 = (PreparedStatement) Con.con.prepareStatement(sqlser);
			ResultSet re = sta1.executeQuery();
			while(re.next()) {
				String pwd = re.getString(3);
				String user_name = re.getString(2);
				if (pwd.equals(password)) {
					this.Check = true;
					this.user_name = user_name;
					System.out.println("Login successfully!");
					this.email = email;
					return 0;
				}

				else {
					System.out.println("Password error!");
					this.Check = false;
					return 2;
				}
			}
		}
		return 3;
	}

	private void Create_Account(Connector Con) throws IOException, SQLException {
		while(true) {
			// 建立一個會員資料table
			InputStreamReader ir = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(ir);
			System.out.println("Please enter your email:");
			ResultSet rs;
			String[] email;
			while(true){ //check email 格式
				String emailcheck = in.readLine();
				try {
					if( !emailcheck.matches("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z]+") ){
						throw new type_exception(Case.Account);
					}
					else {
						email = emailcheck.split("@");
						break;
					}
				}
				catch(type_exception e) {
					System.out.println("Illegal format! Please enter again:");
					System.out.println(e.getMessage());
				}

			}
			String email_address = email[1];
			this.email_id = email[0];
			this.email = email[0] + "@" + email[1];
			rs = Con.con.getMetaData().getTables(null, null, email_id, null);// 找出這個email_id是否被使用過
			if (rs.next() == false) {
				String sql = "create table " + email_id + "(ip_address varchar(100), username varchar(50), password varchar(100), trip_title varchar(200), travelcode varchar(50), travelkey varchar(100), price int, startday varchar(100), endday varchar(100), numberofpeople int, lowerbound int, upperbound int, booking_id int)ENGINE=InnoDB DEFAULT CHARSET=utf8;";
				PreparedStatement statement = Con.con.prepareStatement(sql);
				statement.executeUpdate();
				System.out.println("Please enter your username:");
				String user_name = in.readLine();
				this.user_name = user_name;
				while (true) {
					String pwd;
					System.out.println("Please enter your password: (must be 6 characters or more and contain at least one uppercase character, one lowercase character and one number.)");
					while(true){ //check 密碼格式
						pwd = in.readLine();
						if(pwd.matches("^.*(?=.{6,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*")){
							break;
						}
						System.out.println("Illegal format! Please enter again:");
					}

					System.out.println("Verify your password again:");
					String pwd1 = in.readLine();
					if (pwd.equals(pwd1)) {
						String sql_insert = "insert into " + email_id + "(ip_address, username, password, booking_id) values(?,?,?,?)";
						statement = Con.con.prepareStatement(sql_insert);
						statement.setObject(1, email_address);
						statement.setObject(2, user_name);
						statement.setObject(3, pwd);
						statement.setObject(4, 0);
						int result = statement.executeUpdate();
						break;
					}
					else {
						System.out.println("Password does not match! Please enter again:");
					}
				}
				String Title = new String("Account Checking!");
				String body = new String();
				body = "Hi " + user_name + " !\nWelcome to Smart TravelSystem!";
				String email_to = email_id + "@" + email_address;
				SendEmail send = new SendEmail(email_to, Title, body);
				System.out.println("Account " + user_name + " create successfully");
				System.out.println("Check your email for the confirmation letter ^_^");
				break;

			}
			else {
				System.out.println("This email had been used!");
				this.Check = false;
			}
		}
	}
}
