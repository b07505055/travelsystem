package travelSystem.execution;

import travelSystem.backstage.*;
import travelSystem.execution.exception.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Booking extends Search {

	public Booking(String[] s, int i,String user_name, String email ) throws ClassNotFoundException, IOException, SQLException, type_exception, bookingexception {
		super(s);
		super.user_name = user_name;
		if (i == 1) {
			super.find(user_name, email);
		}
		else if(i == 0) {
			this.booking(s, email);
		}
		else {
			throw new type_exception(Case.Booking);
		}
	}

	public void booking(String[] str, String email) throws ClassNotFoundException, SQLException, bookingexception, IOException {
		this.info = str;
		// connect database "trip" in user "Ben"
		Connector Con = new Connector();

		// 查詢
		int i = 0;
		String trip_title = this.info[0];
		int unbooking_number = 0; //未被預訂的旅遊資料
		int[] date_range = new int[2];
		date_range[0] = Date_Change_Int(this.info[1]); // 範圍始
		date_range[1] = Date_Change_Int(this.info[2]); // 範圍尾

		// 查詢未被預訂的資料
		System.out.println("*Unreserved information below*");
		System.out.println("");
		String sql1 = "select * from tripdataalldynamic WHERE title = ? ";
		PreparedStatement sta1 = (PreparedStatement) Con.con.prepareStatement(sql1);
		sta1.setObject(1, trip_title);
		ResultSet re = sta1.executeQuery();

		Data[] d = new Data[10000];

		while (re.next()){
			d[i] = new Data(re.getString(1),re.getString(2),re.getString(3),Integer.parseInt(re.getString(4)),re.getString(5),re.getString(6),Integer.parseInt(re.getString(7)), Integer.parseInt(re.getString(8)));

			if (Date_Change_Int(d[i].endday) <= date_range[1] && Date_Change_Int(d[i].startday) >= date_range[0]) {
				System.out.println("Title" + (i + 1) + ": " + d[i].title);
				System.out.println("Total price: " + d[i].price );
				System.out.println("Min number of people: " +  d[i].lowerbound);
				System.out.println("Max number of people: " + d[i].upperbound );
				System.out.println("Start date: " + d[i].startday );
				System.out.println("End date: " + d[i].endday );
				System.out.println("");

				i ++;
				unbooking_number ++;
			}

		}

		// 查詢已被預訂的資料

		System.out.println("*Reserved information below*");
		System.out.println("");
		sql1 = "select * from bookingdata WHERE title = ? ";
		sta1 = (PreparedStatement) Con.con.prepareStatement(sql1);
		sta1.setObject(1, trip_title);
		re = sta1.executeQuery();
		while(re.next()) {

			d[i] = new Data(re.getString(2),re.getString(3),re.getString(4),Integer.parseInt(re.getString(5)),re.getString(6),re.getString(7),Integer.parseInt(re.getString(10)), Integer.parseInt(re.getString(11)));
			d[i].booking_id = re.getInt(9);

			if (Date_Change_Int(d[i].endday) <= date_range[1] && Date_Change_Int(d[i].startday) >= date_range[0]) {
				System.out.println("Title" + (i + 1) + ": " + d[i].title);
				System.out.println("Total price: " + d[i].price );
				System.out.println("Min number of people: " +  d[i].lowerbound);
				System.out.println("Max number of people: " + d[i].upperbound );
				System.out.println("Start date: " + d[i].startday );
				System.out.println("End date: " + d[i].endday );
				System.out.println("");


				i ++;
			}
		}

		if (i == 0) {
			System.out.println("We have no relevant travel QQ");
		}
		else {
			System.out.println("Finish searching");
			i = 0;

			int id;
			System.out.println("Do you want to book any travel?(y/n)");
			InputStreamReader ir = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(ir);
			if (in.readLine().equals("y")) {
				this.type = Case.Booking;
				System.out.println("Please enter the travel number.");
				id = Integer.parseInt(in.readLine()) - 1;
				if(id < unbooking_number) {
					this.data = d[id];
					super.booking(this.data, Con, email);
				}

				else {
					System.out.println("此行程已被預訂!\n我們會先將您的資料放入保留區內，一旦原預訂者退訂，將會立馬通知您預訂");
					String sqli = "insert into wating_data(user_id, ip_address, booking_id) values(?,?,?)";
					sta1 = (PreparedStatement) Con.con.prepareStatement(sqli);
					sta1.setObject(1, email.split("@")[0]);
					sta1.setObject(2, email.split("@")[1]);
					sta1.setObject(3, d[id].booking_id);
					int result = sta1.executeUpdate();
					if (result != 0) {
						String To = email;
						String Title = "Preserve Success!";
						String body = "敬愛的"+ user_name +"您好, 已將您感興趣的行程: "+ d[id].title +" 加入到我們的行程保留區。\r\n" + "當原預訂者退訂時，我們將會發送email告知您，感謝您的使用。\r\n";
						SendEmail send = new SendEmail(To, Title, body);
					}
				}
			}
		}

		Con.con.close();
	}

}