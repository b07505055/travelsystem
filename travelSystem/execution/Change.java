package travelSystem.execution;

import travelSystem.backstage.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Change extends Check {

	private int lowerbound;
	private int upperbound;

	public Change(String id, String email, String user_name) throws ClassNotFoundException, SQLException {
		super(id, email, user_name);
	}

	public void alter(int n, String username) throws ClassNotFoundException, SQLException {
		// connect database "trip" in user "Ben"
		Connector Con = new Connector();
		// 搜索bookingdata的內容
		String sql1 = "select * from bookingdata WHERE id = ?";
		PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql1);
		statement.setObject(1, super.id);
		ResultSet re = statement.executeQuery();
		int i = 0;
		int total_price;
		while (re.next()) {
			lowerbound = Integer.parseInt(re.getString(10));
			upperbound = Integer.parseInt(re.getString(11));
			total_price = (Integer.parseInt(re.getString(5))/Integer.parseInt(re.getString(8))) * n;
			if (n >= lowerbound && n <= upperbound) {
				//更新內容
				String sql = "update bookingdata set numberofpeople = ?, price = ? where id = ?";
				statement = (PreparedStatement) Con.con.prepareStatement(sql);
				statement.setObject(1, n);
				statement.setObject(2, total_price);
				statement.setObject(3, super.id);
				int result = statement.executeUpdate();
				if (result == 1) {
					System.out.println("修改成功, 已將您的人數變更為: " + n);
				}

				Con = new Connector("user_account");
				String sqluser = "update " + super.user_id + " set numberofpeople = ?, price = ? where booking_id = ?";
				statement = (PreparedStatement) Con.con.prepareStatement(sqluser);
				statement.setObject(1, n);
				statement.setObject(2, total_price);
				statement.setObject(3, super.id);
				result = statement.executeUpdate();
				if (result != 0) {
					// 搜索table "user_id" 的內容
					String sqll = "select * from " + this.user_id + " WHERE booking_id = ?";
					statement = (PreparedStatement) Con.con.prepareStatement(sqll);
					statement.setObject(1, this.id);
					ResultSet res = statement.executeQuery();
					int ii = 0;
					String travelkey = new String();
					String title = new String();
					String time = new String();
					String price = new String();
					String npeople = new String();
					while (res.next()) {
						travelkey = res.getObject(6).toString();
						title = res.getObject(4).toString();
						time = (res.getObject(8)+ " - " +re.getObject(9)).toString();
						price = res.getObject(7).toString();
						npeople = res.getObject(10).toString();
						ii ++;
					}
					// 發送更改確認函
					String Title = "Change Success!";
					String body = "敬愛的" + username + "您好! 感謝您使用了本系統的更新功能, 以下為您的更正後的資訊\n"+ "行程編號: " + travelkey + "\n" + "行程名稱: " + title + "\n" + "出發日期-抵台日期: " + time + "\n" + "總價: " + price + "\n" + "入住人數: " + npeople;
					SendEmail send = new SendEmail(super.email, Title, body);
				}
			}
			else {
				System.out.println("人數不符合此行程!");
			}
			i ++;
		}
		if (i == 0) {
			System.out.println("查無此訂單，請檢查是否有輸入錯誤!");
		}

		Con.con.close();
	}

	public void refund(String username) throws SQLException, ClassNotFoundException {
		// connect database "trip" in user "Ben"
		Connector Con = new Connector();

		Data d = null;

		//搜索bookingdata，找到預約的旅程資訊
		String sql = "select * from bookingdata WHERE id = ?";
		PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql);
		statement.setObject(1, super.id);
		ResultSet re = statement.executeQuery();
		int price;
		while (re.next()) {
			price = Integer.parseInt(re.getString(5))/Integer.parseInt(re.getString(8)); //價格為總價除人數
			d = new Data(re.getString(2),re.getString(3),re.getString(4),price,re.getString(6),re.getString(7),Integer.parseInt(re.getString(10)), Integer.parseInt(re.getString(11)));
		}

		// 搜索bookingdata的內容，把預約行程刪除
		String sql1 = "delete from bookingdata WHERE id = ?";
		statement = (PreparedStatement) Con.con.prepareStatement(sql1);
		statement.setObject(1, super.id);
		int result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("退訂成功, 已取消您的預訂紀錄");
		}

		// 搜索tripdataall,找到booking的旅遊資訊

		// 把此旅遊資訊上傳回tripdataalldynamic中
		String sql2 ="insert into tripdataalldynamic(title, travelcode, travelkey, price, startday, endday, lowerbound, upperbound) values(?,?,?,?,?,?,?,?)";
		statement = (PreparedStatement) Con.con.prepareStatement(sql2);
		statement.setObject(1, d.title);
		statement.setObject(2, d.travelcode);
		statement.setObject(3, d.travelkey);
		statement.setObject(4, d.price);
		statement.setObject(5, d.startday);
		statement.setObject(6, d.endday);
		statement.setObject(7, d.lowerbound);
		statement.setObject(8, d.upperbound);

		result = statement.executeUpdate();
		if (result == 1){
			System.out.println("資料回傳成功");
		}

		// 發送email告知行程保留者，已經能夠預訂了
		String sql_wating_send = "select * from wating_data where booking_id = ?";
		statement = (PreparedStatement) Con.con.prepareStatement(sql_wating_send);
		statement.setObject(1, super.id);
		ResultSet res = statement.executeQuery();
		while (res.next()) {
			String To = res.getString(1) + "@" + res.getString(2);
			String Title = "You Can Book Now!";
			String body = "Hi! 您感興趣的行程: "+ d.title +" 原預訂者已退訂，您可以趕緊預訂了!";
			SendEmail send = new SendEmail(To, Title, body);
		}

		// 刪除wating_data的資訊
		String sql_wating_de = "delete from wating_data WHERE booking_id = ?";
		statement = (PreparedStatement) Con.con.prepareStatement(sql_wating_de);
		statement.setObject(1, super.id);
		result = statement.executeUpdate();

		// 刪除user的預定資料，並發email告知user
		Con = new Connector("user_account");
		String sqluser = "delete from " + super.user_id + " where booking_id = ?";
		statement = (PreparedStatement) Con.con.prepareStatement(sqluser);
		statement.setObject(1, super.id);
		result = statement.executeUpdate();
		if (result != 0) {
			String Title = "Refund Success!";
			String body = "敬愛的" + username + "您好! 感謝您使用了本系統的退訂功能\n特以此信告知您原行程 " + d.title + " 訂單編號: " + super.id + " 退訂成功了!";
			SendEmail send = new SendEmail(super.email, Title, body);
		}

		Con.con.close();
	}
}
