package travelSystem.execution;

import travelSystem.backstage.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Check {

	public boolean Check_success;
	protected int id;
	protected String user_id;
	protected String email;
	protected String user_name;
	public Check(String id, String email, String user_name) throws ClassNotFoundException, SQLException {
		this.id = Integer.parseInt(id);
		this.email = email;
		this.user_id = email.split("@")[0];
		this.user_name = user_name;
		check_booking(this.id, this.user_id);
	}

	public Check(int booking_id, String email, String user_name) throws ClassNotFoundException, SQLException {
		this.id = booking_id;
		this.email = email;
		this.user_id = email.split("@")[0];
		this.user_name = user_name;
		check_booking(this.id, this.user_id);
	}

	public void Check_mail(String username) throws ClassNotFoundException, SQLException{
		// connect database "user_account" in user "Ben"
		Connector Con = new Connector("user_account");

		// 搜索table "user_id" 的內容
		String sql1 = "select * from " + this.user_id + " WHERE booking_id = ?";
		PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql1);
		statement.setObject(1, this.id);
		ResultSet re = statement.executeQuery();
		int i = 0;
		String travelkey = new String();
		String title = new String();
		String time = new String();
		String price = new String();
		String npeople = new String();
		while (re.next()) {
			travelkey = re.getObject(6).toString();
			title = re.getObject(4).toString();
			time = (re.getObject(8)+ " - " +re.getObject(9)).toString();
			price = re.getObject(7).toString();
			npeople = re.getObject(10).toString();
			i ++;
		}
		if (i == 0) {
			System.out.println("查無此訂單，請確認是否有輸入錯誤!");
		}
		String To = this.email;
		String Title = "Booking success!";
		String body = "Dear "  + username + ": Hi! Thank you for using our reservation system! these are your reservation information\n"+ "Reservation number: "+ this.id + "\n" +"Travel number: " + travelkey + "\n" + "Title: " + title + "\n" + "Start date-End date: " + time + "\n" + "Total price: " + price + "\n" + "Signed up number of people: " + npeople;
		SendEmail send = new SendEmail (To, Title, body);
		Con.con.close();
	}

	public void check_booking(int id, String user_id) throws ClassNotFoundException, SQLException {

		// connect database "user_account" in user "Ben"
		Connector Con = new Connector("user_account");

		// 搜索table "user_id" 的內容，並把預約資訊print出來
		if (id != 0) {
			String sql1 = "select * from " + user_id + " WHERE booking_id = ?";
			PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql1);
			statement.setObject(1, id);
			ResultSet re = statement.executeQuery();
			int i = 0;
			while (re.next()) {
				System.out.println("Username: " + this.user_name);
				System.out.println("Reservation number: "+ id);
				System.out.println("Travel number : " + re.getObject(6));
				System.out.println("Title: " + re.getObject(4));
				System.out.println("Start date-End date: " + re.getObject(8)+ "-" +re.getObject(9));
				System.out.println("Total price: " + re.getObject(7));
				System.out.println("Range of number of people: " + re.getObject(11) + "-" + re.getObject(12));
				System.out.println("Signed up number of people: " + re.getObject(10));
				i ++;
				this.Check_success = true;
			}
			if (i == 0) {
				System.out.println("查無此訂單，請確認是否有輸入錯誤!");
				this.Check_success = false;
			}
		}
		else {
			String sql1 = "select * from " + user_id ;
			PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql1);
			ResultSet re = statement.executeQuery();
			int i = 0;
			while (re.next()) {
				System.out.println("Username: " + this.user_name);
				System.out.println("Reservation number: "+ re.getObject(13));
				System.out.println("Travel number : " + re.getObject(6));
				System.out.println("Title: " + re.getObject(4));
				System.out.println("Start date-End date: " + re.getObject(8)+ "-" +re.getObject(9));
				System.out.println("Total price: " + re.getObject(7));
				System.out.println("Range of number of people: " + re.getObject(11) + "-" + re.getObject(12));
				System.out.println("Signed up number of people : " + re.getObject(10));
				i ++;
				this.Check_success = true;
				System.out.println("");
			}

			if (i == 0) {
				System.out.println("查無此訂單，請確認是否有輸入錯誤!");
				this.Check_success = false;
			}
		}
		Con.con.close();
	}
}
