package travelSystem.execution;

import java.io.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import java.util.ArrayList;
import travelSystem.backstage.*;
import travelSystem.execution.exception.bookingexception;

public class Search {

	protected String travel_code;
	protected String[] info ;
	protected Data data;
	protected String user_name;
	public Case type;

	public Search(String[] s) {
		this.info = s;
		this.type = Case.Search;
	}

	public static String[] traversal_initial(String distrcit) {
		JSONParser parser = new JSONParser();
		ArrayList<String> area = new ArrayList<String>();
		String[] area_array = new String[1];
		try {

			Object obj = parser.parse(new InputStreamReader(new FileInputStream("C:/Users/travel_code_change_2.json"), "UTF-8"));

			JSONArray companyList = (JSONArray) obj;
			JSONObject[] obj2 = new JSONObject[companyList.toArray().length];

			// 把整個json用一個array包起來
			for (int i = 0; i < companyList.toArray().length; i++) {
				obj2[i] = (JSONObject) companyList.get(i);
			}

			// 讀資料
			// 找到travel_district值為district的地區
			for (int j = 0; j < obj2.length; j++) {

				if (obj2[j].get("travel_district").toString().equals(distrcit)) {

					for (int k = 0; k < obj2[j].get("travel_code_name").toString().split("．").length; k ++) {
						area.add(obj2[j].get("travel_code_name").toString().split("．")[k]);
					}
				}

			}

			area_array = new String[area.size()];

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			return area.toArray(area_array);
		}
	}

	protected void traversal() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new InputStreamReader(new FileInputStream("C:/Users/travel_code.json"), "UTF-8"));

			JSONArray companyList = (JSONArray) obj;
			JSONObject[] obj2 = new JSONObject[companyList.toArray().length];
			for (int i = 0; i < companyList.toArray().length; i++) {
				obj2[i] = (JSONObject) companyList.get(i);
			}

			for (int j = 0; j < obj2.length; j++) {

				for (int k = 0; k < obj2[j].get("travel_code_name").toString().split("．").length; k ++) {
					if (obj2[j].get("travel_code_name").toString().split("．")[k].equals(this.info[0])) {
						this.travel_code = obj2[j].get("travel_code").toString();
						break;
					}
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void find(String user_name ,String email) throws IOException, ClassNotFoundException, SQLException, bookingexception{

		this.traversal();
		this.user_name = user_name;
		// connect database "trip" in user "Ben"
		Connector Con = new Connector();

		// 查詢
		int i = 0;
		int unbooking_number = 0; //未被預訂的旅遊資料
		int[] date_range = new int[2];
		date_range[0] = Date_Change_Int(this.info[1]); // 範圍始
		date_range[1] = Date_Change_Int(this.info[2]); // 範圍尾

		// 查詢未被預訂的資料
		System.out.println("*以下為尚未被預訂的資料*");
		System.out.println("");
		String sql1 = "select * from tripdataalldynamic WHERE travelcode = ? ";
		PreparedStatement sta1 = (PreparedStatement) Con.con.prepareStatement(sql1);
		sta1.setObject(1, this.travel_code);
		ResultSet re = sta1.executeQuery();

		Data[] d = new Data[10000];

		while (re.next()){
			d[i] = new Data(re.getString(1),re.getString(2),re.getString(3),Integer.parseInt(re.getString(4)),re.getString(5),re.getString(6),Integer.parseInt(re.getString(7)), Integer.parseInt(re.getString(8)));

			if (Date_Change_Int(d[i].endday) <= date_range[1] && Date_Change_Int(d[i].startday) >= date_range[0]) {
				System.out.println("行程" + (i + 1) + ": " + d[i].title);
				System.out.println("價格: " + d[i].price );
				System.out.println("最少出團人數: " +  d[i].lowerbound);
				System.out.println("最多出團人數: " + d[i].upperbound );
				System.out.println("出發日期: " + d[i].startday );
				System.out.println("回台日期: " + d[i].endday );
				System.out.println("");

				i ++;
				unbooking_number ++;
			}

		}

		// 查詢已被預定的資料
		System.out.println("*以下為已被預訂的資料*");
		System.out.println("");
		sql1 = "select * from bookingdata WHERE travelcode = ? ";
		sta1 = (PreparedStatement) Con.con.prepareStatement(sql1);
		sta1.setObject(1, this.travel_code);
		re = sta1.executeQuery();
		while(re.next()) {

			d[i] = new Data(re.getString(2),re.getString(3),re.getString(4),Integer.parseInt(re.getString(5)),re.getString(6),re.getString(7),Integer.parseInt(re.getString(10)), Integer.parseInt(re.getString(11)));
			d[i].booking_id = re.getInt(9);

			if (Date_Change_Int(d[i].endday) <= date_range[1] && Date_Change_Int(d[i].startday) >= date_range[0]) {

				System.out.println("行程" + (i + 1) + ": " + d[i].title);
				System.out.println("價格: " + d[i].price );
				System.out.println("最少出團人數: " +  d[i].lowerbound);
				System.out.println("最多出團人數: " + d[i].upperbound );
				System.out.println("出發日期: " + d[i].startday );
				System.out.println("回台日期: " + d[i].endday );
				System.out.println("");

				i ++;
			}
		}

		if (i == 0) {
			System.out.println("本公司未提供相關的旅程QQ");
		}
		else {
			System.out.println("查詢完畢");
			i = 0;

			int id;
			System.out.println("Do you want to book any travel?(y/n)");
			InputStreamReader ir = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(ir);
			if (in.readLine().equals("y")) {
				this.type = Case.Booking;
				System.out.println("Please enter the travel number.");
				id = Integer.parseInt(in.readLine()) - 1;
				if(id < unbooking_number) {
					this.data = d[id];
					booking(this.data, Con, email);
				}

				else {
					System.out.println("此行程已被預訂!\n我們會先將您的資料放入保留區內，一旦原預訂者退訂，將會立馬通知您!");
					String sqli = "insert into wating_data(user_id, ip_address, booking_id) values(?,?,?)";
					sta1 = (PreparedStatement) Con.con.prepareStatement(sqli);
					sta1.setObject(1, email.split("@")[0]);
					sta1.setObject(2, email.split("@")[1]);
					sta1.setObject(3, d[id].booking_id);
					int result = sta1.executeUpdate();
					if (result != 0) {
						String To = email;
						String Title = "Preserve Success!";
						String body = "敬愛的"+ user_name +"您好, 已將您感興趣的行程: "+ d[id].title +" 加入到我們的行程保留區。\r\n" + "當原預訂者退訂時，我們將會發送email告知您，感謝您的使用。\r\n";
						SendEmail send = new SendEmail(To, Title, body);
					}
				}
			}
		}

		Con.con.close();
	}

	protected void booking(Data d, Connector Con, String email) throws bookingexception, IOException, SQLException, ClassNotFoundException{

		String user_id = email.split("@")[0];
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);
		String s = new String();

		while (!s.equals("n")) {
			try {
				System.out.println("Please enter the information on next line.");
				System.out.println("入住人數");
				String people_number = in.readLine();
				if (Integer.parseInt(people_number) <= d.upperbound && Integer.parseInt(people_number) >= d.lowerbound) {

					// 將預約資訊上傳到bookingdata
					String sql ="insert into bookingdata(userid, title, travelcode, travelkey, price, startday, endday, numberofpeople, lowerbound, upperbound) values(?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement statement = (PreparedStatement) Con.con.prepareStatement(sql);
					int total_price = d.price * Integer.parseInt(people_number);
					statement.setObject(1, user_id);
					statement.setObject(2, d.title);
					statement.setObject(3, d.travelcode);
					statement.setObject(4, d.travelkey);
					statement.setObject(5, total_price);
					statement.setObject(6, d.startday);
					statement.setObject(7, d.endday);
					statement.setObject(8, Integer.parseInt(people_number));
					statement.setObject(9, d.lowerbound);
					statement.setObject(10, d.upperbound);

					int result1 = statement.executeUpdate();

					// 搜索bookingdata的內容，並把預約資訊print出來
					String sql1 = "select * from bookingdata WHERE title = ? and travelkey = ? and startday = ? ";
					statement = (PreparedStatement) Con.con.prepareStatement(sql1);
					statement.setObject(1, d.title);
					statement.setObject(2, d.travelkey);
					statement.setObject(3, d.startday);
					ResultSet re = statement.executeQuery();
					int booking_id = 0;
					while (re.next()) {
						booking_id = re.getInt(9);
					}

					// 將被預約的行程從tripdataalldynamic刪除
					String sq2 ="delete from tripdataalldynamic where title = ? and travelkey = ? and startday = ?";
					statement = (PreparedStatement) Con.con.prepareStatement(sq2);
					statement.setObject(1, d.title);
					statement.setObject(2, d.travelkey);
					statement.setObject(3, d.startday);
					result1 = statement.executeUpdate();
					System.out.println(result1);
					if (result1 != 0) {
						//System.out.println("刪除成功");
					}

					// 將預約資訊傳到UserAccount
					Connector Con_AC = new Connector("user_account");
					String sqlA ="insert into "+ user_id +"(trip_title, travelcode, travelkey, price, startday, endday, numberofpeople, lowerbound, upperbound, booking_id) values(?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement statementA = (PreparedStatement) Con_AC.con.prepareStatement(sqlA);
					statementA.setObject(1, d.title);
					statementA.setObject(2, d.travelcode);
					statementA.setObject(3, d.travelkey);
					statementA.setObject(4, total_price);
					statementA.setObject(5, d.startday);
					statementA.setObject(6, d.endday);
					statementA.setObject(7, Integer.parseInt(people_number));
					statementA.setObject(8, d.lowerbound);
					statementA.setObject(9, d.upperbound);
					statementA.setObject(10, booking_id);

					int result2 = statementA.executeUpdate();
					Con.con.close();
					if (result1 != 0 && result2 != 0){
						Check check = new Check(booking_id, email, user_name);
						check.Check_mail(user_name); // 寄送確認信件
						break;
					}
				}
				else {
					throw new bookingexception(0);
				}
			}
			catch(bookingexception e) {
				System.out.println(e.getMessage());
				System.out.println("Continue? (y/n)");
				s = in.readLine();
			}
		}

	}

	//把日期變成一個整數(日期越大數字越大)
	public static int Date_Change_Int (String date){
		//2020-01-01為基準(第一天)
		int value = 0;
		int year_0 = 2020;
		int month_0 = 1;
		int day_0 = 1;
		String[] Date = date.split("-"); // year, month, day
		int year = Integer.parseInt(Date[0]);
		int month = Integer.parseInt(Date[1]);
		int day = Integer.parseInt(Date[2]);

		while (year_0 != year) {
			if (year_0 < year) {
				value += 365;
				year_0++;
			}
			else{
				value -= 365;
				year_0--;
			}
		}
		while (month_0 != month) {
			if (month_0 == 1 || month_0 == 3 || month_0 == 5 || month_0 == 7 || month_0 == 8 || month_0 == 10 || month_0 == 12) {
				value += 31;
			}
			else if (month_0 == 2){
				value += 28;
			}
			else {
				value += 30;
			}
			month_0++;
		}

		while (day_0 != day) {
			value++;
			day_0++;
		}

		return value;
	}
}
