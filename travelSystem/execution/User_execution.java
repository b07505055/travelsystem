package travelSystem.execution;
import travelSystem.backstage.*;
import travelSystem.execution.exception.*;
import java.io.*;
import java.sql.SQLException;

public class User_execution {
	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);
		String[] User_information = User_execution.Sign_in();
		String user_name = User_information[0];
		String email = User_information[1];
		int i; // i 是決定要使用哪個模式的變數
		String sss = new String("0");

		while( !sss.equals("n") ) {

			System.out.println("");
			System.out.println("Please enter the ID chosen by you.");
			System.out.println("1. 查詢可報名行程\n"
					+ "2. 特定行程預訂\n"
					+ "3. 行程退訂與修改\n"
					+ "4. 查詢已報名行程\n");

			try {
				String s = new String();
				s = in.readLine();

				i = Integer.parseInt(s); // i 是決定要使用哪個模式的變數

				Case c;
				switch (i) {
					case 1:
						c = Case.Search;
						break;
					case 2:
						c = Case.Booking;
						break;
					case 3:
						c = Case.Change;
						break;
					case 4:
						c = Case.Check;
						break;
					default :
						throw new type_exception(Case.inte); //如果打了不在範圍內的工作就丟例外

				}

				String[] ss = new String[10];   //用來判斷輸入是否有誤的字串陣列
				String str = new String();
				switch (c) {


					case Search:
						while( !str.equals("n")) {
							try {
								System.out.println("Where do you want to go?");
								System.out.println("東北亞(1), 東南亞(2), 港澳大陸(3), 南亞中東(4), 美洲(5), 紐澳(6), 歐洲(7), 非洲(8), 太平洋島嶼(9)");
								int district = Integer.parseInt(in.readLine());
								switch (district) {
									case 1:
										System.out.println("日本，韓國");
										break;

									case 2:
										System.out.println("泰國，新加坡，馬來西亞，菲律賓，印尼，中南半島");
										break;

									case 3:
										System.out.println("華北，東北，華東，華中，華南，西南，塞外邊疆，港澳");										break;

									case 4:
										System.out.println("印度週邊，西亞，馬爾地夫");
										break;

									case 5:
										System.out.println("美國，加拿大，中美洲，南美洲");
										break;

									case 6:
										System.out.println("澳洲，紐西蘭");
										break;

									case 7:
										System.out.println("中歐，西歐，南歐，北歐，東歐，俄羅斯");
										break;

									case 8:
										System.out.println("非洲，馬達加斯加");
										break;

									case 9:
										System.out.println("太平洋島嶼");
										break;
								}
								String Country = in.readLine();
								String[] area = Search.traversal_initial(Country);
								System.out.println("These are the areas you can choose.");
								for (int ii = 0; ii < area.length; ii ++) {
									System.out.print(area[ii] + " ");
								}
								System.out.println("");
								System.out.println("");
								System.out.println("Please enter the information on next line.");
								System.out.println("目的地_遊玩日期範圍(ex: 2020-01-20_2020-02-20)");
								s = in.readLine();
								ss = s.split("_");
								if (ss.length != 3) throw new type_exception(Case.Search);
								if (ss[1].split("-").length != 3 || ss[1].split("-")[0].length() != 4 || ss[1].split("-")[1].length() != 2 || ss[1].split("-")[2].length() != 2 || ss[2].split("-").length != 3 || ss[2].split("-")[0].length() != 4 || ss[2].split("-")[1].length() != 2 || ss[2].split("-")[2].length() != 2) {
									throw new type_exception(Case.Search);
								}
								else {
									Search search = new Search(ss);
									search.find(user_name, email);
									System.out.println("Do you want to search continue? (y/n)");
									str = in.readLine();
								}
							}
							catch(type_exception te) {
								System.out.println(te.getMessage());
							}
						}

						break;

					case Booking:
						while( !str.equals("n")) {
							try {
								System.out.println("請問您知道您要預訂的行程名稱嗎?(y/n)");
								if(in.readLine().equals("y")) {
									System.out.println("Please enter the information on next line.");
									System.out.println("行程名稱_遊玩日期範圍(ex:【波蘭、波羅的海三小國、俄羅斯】精彩12 日_2020-01-20_2020-02-20)");
									s = in.readLine();
									ss = s.split("_");

									if (ss.length != 3) throw new type_exception(Case.Booking);
									else {
										Booking b = new Booking(ss, 0, user_name, email);
										System.out.println("Do you want to book continue? (y/n)");
										str = in.readLine();
									}
								}
								else {
									try {
										System.out.println("Where do you want to go?");
										System.out.println("東北亞(1), 東南亞(2), 港澳大陸(3), 南亞中東(4), 美洲(5), 紐澳(6), 歐洲(7), 非洲(8), 太平洋島嶼(9)");
										int district = Integer.parseInt(in.readLine());
										switch (district) {
											case 1:
												System.out.println("日本，韓國");
												break;

											case 2:
												System.out.println("泰國，新加坡，馬來西亞，菲律賓，印尼，中南半島");
												break;

											case 3:
												System.out.println("華北，東北，華東，華中，華南，西南，塞外邊疆，港澳");										break;

											case 4:
												System.out.println("印度週邊，西亞，馬爾地夫");
												break;

											case 5:
												System.out.println("美國，加拿大，中美洲，南美洲");
												break;

											case 6:
												System.out.println("澳洲，紐西蘭");
												break;

											case 7:
												System.out.println("中歐，西歐，南歐，北歐，東歐，俄羅斯");
												break;

											case 8:
												System.out.println("非洲，馬達加斯加");
												break;

											case 9:
												System.out.println("太平洋島嶼");
												break;
										}
										String Country = in.readLine();
										String[] area = Search.traversal_initial(Country);
										System.out.println("These are the areas you can choose.");
										for (int ii = 0; ii < area.length; ii ++) {

											System.out.print(area[ii] + " ");

										}
										System.out.println("");
										System.out.println("");
										System.out.println("Please enter the information on next line.");
										System.out.println("目的地_遊玩日期範圍(ex: 2020-01-20_2020-02-20)");
										s = in.readLine();
										ss = s.split("_");
										if (ss.length != 3) throw new type_exception(Case.Search);
										if (ss[1].split("-").length != 3 || ss[1].split("-")[0].length() != 4 || ss[1].split("-")[1].length() != 2 || ss[1].split("-")[2].length() != 2 || ss[2].split("-").length != 3 || ss[2].split("-")[0].length() != 4 || ss[2].split("-")[1].length() != 2 || ss[2].split("-")[2].length() != 2) {
											throw new type_exception(Case.Search);
										}
										else {
											Search search = new Search(ss);
											search.find(user_name, email);
											System.out.println("Do you want to book continue? (y/n)");
											str = in.readLine();
										}
									}
									catch(type_exception te) {
										System.out.println(te.getMessage());
									}

								}


							}
							catch(type_exception te) {
								System.out.println(te.getMessage());
							}
						}

						break;

					case Change:
						String t = new String();
						while( !str.equals("n")) {
							System.out.println("Please enter the information on next line.");
							System.out.println("訂單編號 (輸入0查看所有訂單)");
							String id = in.readLine();
							if(id.equals("0")){
								Change change = new Change(id, email, user_name);
								System.out.println("請選擇您要修改的訂單編號");
								String booking_id = in.readLine();
								change = new Change(booking_id, email, user_name);
								if (change.Check_success) {
									System.out.println("您要 退票(r)/修改人數(a)?");
									t = in.readLine();
									if (t.equals("r")) {
										change.refund(user_name);
									}
									else if (t.equals("a")) {
										System.out.println("請重新輸入您的入住人數:");
										int number = Integer.parseInt(in.readLine());
										change.alter(number, user_name);
									}

									System.out.println("Do you want to change continue? (y/n)");
									str = in.readLine();
								}
							}
							else {
								Change change = new Change(id, email, user_name);
								if (change.Check_success) {
									System.out.println("您要 退票(r)/修改人數(a)?");
									t = in.readLine();
									if (t.equals("r")) {
										change.refund(user_name);
									} else if (t.equals("a")) {
										System.out.println("請重新輸入您的入住人數:");
										int number = Integer.parseInt(in.readLine());
										change.alter(number, user_name);
									}

									System.out.println("Do you want to change continue? (y/n)");
									str = in.readLine();
								}
							}
						}

						break;

					case Check:
						while( !str.equals("n")) {
							System.out.println("Please enter the information on next line.");
							System.out.println("訂單編號 (輸入0查看所有訂單)");
							String id = in.readLine();
							Check check = new Check(id, email, user_name);
							System.out.println("Do you want check continue? (y/n)");
							str = in.readLine();
						}
						break;
				}

			}
			catch(type_exception e){
				System.out.println(e.getMessage());
			}

			System.out.println("Do you want to use travel_system continue? (y/n)");
			sss = in.readLine();

		}
		System.out.println("See you next time!");
	}

	public static String[] Sign_in() throws ClassNotFoundException, IOException, SQLException {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);
		String user_id = new String();
		System.out.println("Login or create an account?(L/C)");
		String acc = in.readLine();
		if (acc.equals("L") || acc.equals("l")) {
			System.out.println("Please enter your account_ID(email)");
			String email = in.readLine();
			System.out.println("Please enter your password");
			String password = in.readLine();
			UserAccount oac = new UserAccount(email, password); //Check Account valuable.
			String[] info = new String[2] ;
			info[0] = oac.Get_user_name();
			info[1] = oac.Get_user_email();
			return info;
		}
		else if (acc.equals("C") || acc.equals("c")){
			System.out.println("Do you want to create an account?(y/n)");
			if (in.readLine().equals("y")) {
				UserAccount nac = new UserAccount(); // 幫這個使用者創建帳號
				String[] info = new String[2];
				info[0] = nac.Get_user_name();
				info[1] = nac.Get_user_email();
				return info;
			}
			else {
				System.out.println("See you next time~");
				System.exit(0);
			}
		}
		else {
			System.out.println("See you next time~");
			System.exit(0);
		}
		return null;
	}

}
