package travelSystem.execution.exception;

public class bookingexception extends Exception {
	private int id;

	public bookingexception(int id) {
		this.id = id;
	}

	public bookingexception() {
		this.id = 0;
	}

	public String getMessage() {
		String s = new String();
		switch (this.id) {
			case 0:
				s = "您的預定資訊不符合此行程，請更換行程或是重新輸入一次!";
				break;
		}
		return s;
	}
}
