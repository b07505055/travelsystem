package travelSystem.execution.exception;
import travelSystem.execution.*;
public class type_exception extends Exception{
	private Case c;
	public type_exception(Case c){
		this.c = c;
	}

	public String getMessage() {

		String s = new String();
		switch (this.c) {
			case inte:
				s = "Number is out of range!";
				break;

			case Search:
				s = "input format: Destination_Range of travel date(ex: 捷克_2020-01-20_2020-02-20)";
				break;

			case Booking:
				s = "input format: Title  of travel _Range of travel date(ex:【波蘭、波羅的海三小國、俄羅斯】精彩12 日_2020-01-20_2020-02-20)";
				break;

			case Change:
				s = "input format: account_ID(email) 訂單編號(111)";
				break;

			case Check:
				s = "input format: account_ID(email) 訂單編號(111)";
				break;

			case Account:
				s = "Please enter your password with the given format";
		}
		return s;
	}
}
